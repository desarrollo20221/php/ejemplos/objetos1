<div>
    <h2>Añadimos metodo magico destructor</h2>
</div>

<?php

// creamos una clase
class Alumno{
    // propiedades de la clase
        
    // visibilidad nombrePropiedad
    public $nombre;
    public $apellido;
    public $edad;
    
    // metodos magicos de la clase
    // son metodos que se ejecutan automaticamente
    // ante determinadas condiciones
    public function __construct($nombre, $apellido, $edad) {
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->edad = $edad;
    } 
    
    // metodo magico que se llama cuando se elimina el objeto
    // con la funcion unset
    public function __destruct() {
        echo "Destruido";
    }
        
    // este metodo magico se llamara cuando intente imprimir un objeto
    public function __toString() {
        return "{$this->nombre}, {$this->apellido}, {$this->edad}";
    }
    
    
    //metodos getters y setters
    public function getNombre() {
        return $this->nombre;
    }

    public function getApellido() {
        return $this->apellido;
    }

    public function getEdad() {
        return $this->edad;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this; // para que sea fluent
    }

    public function setApellido($apellido) {
        $this->apellido = $apellido;
        return $this;
    }

    public function setEdad($edad) {
        $this->edad = $edad;
        return $this;
    }

    

    // metodos de la clase
    
    // visibilidad function nombreMetodo(argumento)
    public function saludar(){      
        return "Hola clase<br>";
    }
    public function presentacion() {
        return "Hola mi nombre es $this->nombre y mi apellido es $this->apellido<br>";
    }
    
} 

// para poder utilizar la clase tengo que generar una instancia

// creamos un objeto de tipo alumno
$alumno1 = new Alumno("Ana","Vazquez",40); // cuando creo el objeto se llama al constructor


// acceder al metodo presentacion 
// para mostrar en pantalla
echo $alumno1->presentacion();

var_dump($alumno1); // estamos depurando en objeto alumno1

// cambiar nombre y edad sin fluent
$alumno1->setNombre("Luis"); // $alumno1->nombre="Luis";
$alumno1->setEdad(41);

var_dump($alumno1);

// con fluent
$alumno1->setNombre("Luisa")->setEdad(45);

echo $alumno1->presentacion();

echo $alumno1; // al intentar imprimir el alumno llama a __toString

// al final del codigo se llama a los destructores