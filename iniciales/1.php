<div>
    <h2>Crear clase y objeto</h2>
</div>

<?php

// creamos una clase
class Alumno{
    // propiedades de la clase
        
    // visibilidad nombrePropiedad
    public $nombre="Ana";
    public $apellido="Vazquez";
    public $edad=40;
    
    // metodos de la clase
    
    // visibilidad function nombreMetodo(argumento)
    public function saludar(){      
        return "Hola clase<br>";
    }
    public function presentacion() {
        return "Hola mi nombre es $this->nombre y mi apellido es $this->apellido<br>";
    }
    
} 

// para poder utilizar la clase tengo que generar una instancia

// creamos un objeto de tipo alumno
$alumno1 = new Alumno();

// accediendo a la propiedad nombre del objeto
echo "El nombre del alumno es {$alumno1->nombre} y mi apellido es {$alumno1->apellido}<br>";

// acceder al metodo saludar
// para mostrar en pantalla el saludo
echo $alumno1->saludar();

// acceder al metodo presentacion 
// para mostrar en pantalla
echo $alumno1->presentacion();

