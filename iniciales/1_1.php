<div>
    <h2>Hemos utilizado metodo magico constructor</h2>
</div>

<?php

// creamos una clase
class Alumno{
    // propiedades de la clase
        
    // visibilidad nombrePropiedad
    public $nombre;
    public $apellido;
    public $edad;
    
    // metodos magicos de la clase
    // son metodos que se ejecutan automaticamente
    // ante determinadas condiciones
    public function __construct($nombre, $apellido, $edad) {
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->edad = $edad;
    }        
    
    // metodos de la clase
    
    // visibilidad function nombreMetodo(argumento)
    public function saludar(){      
        return "Hola clase<br>";
    }
    public function presentacion() {
        return "Hola mi nombre es $this->nombre y mi apellido es $this->apellido<br>";
    }
    
} 

// para poder utilizar la clase tengo que generar una instancia

// creamos un objeto de tipo alumno
$alumno1 = new Alumno("Ana","Vazquez",40); // cuando creo el objeto se llama al constructor


// acceder al metodo presentacion 
// para mostrar en pantalla
echo $alumno1->presentacion();

var_dump($alumno1);        

