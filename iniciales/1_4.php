<?php
// crear un clase denominada coche
// con las siguientes propiedades
/*
 * tipo
 * matricula
 * cilindrada
 * fechaCompra
 * 
 */
// Metodos
// crear los getter y setter para las propiedades
// setters fluent
// constructor solo con la inicializacion de la matricula
// metodo toString para imprimir matricula y tipo en una lista

class Coche{
    // visibilidad (public,private,protected)
    public $tipo;
    private $matricula; // la matricula es privada
    public $cilindrada;
    public $fechaCompra;
    
    public function __construct($matricula) {
        $this->matricula = $matricula;
    }
    
    public function __toString() {
        $salida = "<ul>";
        $salida .= "<li>{$this->matricula}</li>";
        $salida .= "<li>{$this->tipo}</li>";
        $salida .= "</ul>";
        return $salida;
    }
    
    
    public function getTipo() {
        return $this->tipo;
    }

    public function getMatricula() {
        return $this->matricula;
    }

    public function getCilindrada() {
        return $this->cilindrada;
    }

    public function getFechaCompra() {
        return $this->fechaCompra;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
        return $this;
    }

    public function setMatricula($matricula) {
        $this->matricula = $matricula;
        return $this;
    }

    public function setCilindrada($cilindrada) {
        $this->cilindrada = $cilindrada;
        return $this;
    }

    public function setFechaCompra($fechaCompra) {
        $this->fechaCompra = $fechaCompra;
        return $this;
    }    
}

// probar mi clase
$coche1 = new Coche("3421DFG");

echo $coche1; // imprimo el coche1 (toString)

var_dump($coche1);

// inicializar el tipo
$coche1->tipo="Turismo"; // puedo realizarlo porque la propiedad es publica

// inicializando el tipo desde su setter
$coche1->setFechaCompra("2022/05/01"); // puedo realizarlo aunque la propiedad fuera privada

//modifica la matricula
$coche1->setMatricula("1111AAA"); 

var_dump($coche1);



