<?php
spl_autoload_register(function($nombreClase){    
    require_once "$nombreClase.php";
});
?>

<?php
    // creando un coche nuevo
    $coche1 = new \clases\objetos\Coche("5826KLJ");
    // coloco una cilindrada al coche 
    $coche1->setCilindrada(1800);
    // coloco un tipo al coche
    $coche1->setTipo("Turismo");
    
    // asigno el coche1 a un coche2
    $coche2 = $coche1; // los objetos se asignan por referencia
    
    $coche1->setMatricula("0000AAA");
    
    // muestro la informacion de los coches
    var_dump($coche2);
    var_dump($coche1);
    
    // clonamos el objeto coche1 al coche3
    $coche3 = clone $coche1;
    $coche3->setMatricula("3333CCC");

    // muestro la informacion de los coches
    var_dump($coche3);
    var_dump($coche2);
    var_dump($coche1);