<?php

spl_autoload_register(function($nombreClase){    
    require_once "$nombreClase.php";
});

// importo la clase perro
use clases\animales\Perro;

// creo un perro
// Lo llamo Thor, Pastor aleman
$perro1 = new Perro("Pastor aleman","Thor");
// muetro la imformacion sobre el perro creado con var_dump
var_dump($perro1);

// imprimo perro1
echo $perro1;

// mostrar un perro
echo $perro1->mostrar();
// mostrar un perro
echo $perro1->mostrar();

// creo un coche
// la no tener importada la clase necesito indicarle el espacio de nombres
$coche1 = new clases\objetos\Coche("2341MMM");
// muestro informacion sobre el coche creado con var_dump
var_dump($coche1);