<?php
namespace clases\animales;
/*
 * Crear una clase denominada perro
 * Colocar el espacio de nombres
 *
 * propiedades privadas
 * raza
 * nombre
 * fechaNacimiento
 *
 * metodos getter y setter (con fluent)
 *
 * metodo constructor para inicializar nombre y raza y fecha de nacimiento
 * La fecha de nacimiento si no pasamos nada debe colocar la fecha de hoy
 *
 * metodo toString que retorna todas las propiedades separadas por comas
 *
 * metodo publico ladrar que retorna "guau guau"
 *
 * metodo public mostrar que dibuja un perro con fontawesome
 *
 */

class Perro {
    private $raza;
    private $nombre;
    private $fechaNacimento;
    
    public function __construct($raza, $nombre, $fechaNacimento="") {
        $this->raza = $raza;
        $this->nombre = $nombre;
        $this->fechaNacimento = $fechaNacimento == "" ? $this->fechaHoy() : $fechaNacimento;
    }
    
    private function fechaHoy(){
        return date("d/m/Y");
    }
    
    public function __toString() {
        //return "{$this->nombre}, {$this->raza}, {$this->fechaNacimento}";
        
        $salida = "";
        $salida = get_object_vars($this); // obtener todas las propiedades del objeto en un array asociativo
        return implode(",", $salida);

    }
    
    public function ladrar() {
        return "guau guau";
    }

    public function mostrar(){
        require_once "fontawesome.inc"; //cargo la libreria

        return '<i class="fa-solid fa-dog"></i>';

    }
    
    
    public function getRaza() {
        return $this->raza;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getFechaNacimento() {
        return $this->fechaNacimento;
    }

    public function setRaza($raza) {
        $this->raza = $raza;
        return $this;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    public function setFechaNacimento($fechaNacimento) {
        $this->fechaNacimento = $fechaNacimento;
        return $this;
    }
}
