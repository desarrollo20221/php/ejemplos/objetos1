<?php

namespace clases\animales;

// Crear la clase empleado que hereda todo de persona
// Ademas tiene las propiedades
// sueldo
// empresa
// crear un constructor que inicializa sueldo y empresa
// Ademas debe llamar al constructor del padre para inicializar nombre, apellidos y edad
// crear getter y setter fluent
// crear un metodo publico denominado presentar que devuelve en una lista el
// nombre
// apellidos
// edad
// sueldo
// empresa
// crear el metodo toString que devuelve las siguientes propiedades separadas por comas
// nombre
// apellidos
// edad
// sueldo
// empresa

class Empleado extends Persona{
    
    public $sueldo;
    public $empresa;
    
    public function __construct($sueldo, $empresa, $nombre, $apellidos, $edad) {
        $this->sueldo = $sueldo;
        $this->empresa = $empresa;
        parent::__construct($nombre, $apellidos, $edad);
    }
    
     public function __toString() {
        $salida = "";
        $salida = get_object_vars($this);
        return implode(",", $salida);
    }
    
    public function presentar() {
        $salida = "<ul>";
        foreach ($this as $valor) {
            $salida .= "<li>{$valor}</li>";
        }
        $salida .= "</ul>";
        return $salida;
    }


 
    public function getSueldo() {
        return $this->sueldo;
    }

    public function getEmpresa() {
        return $this->empresa;
    }

    public function setSueldo($sueldo) {
        $this->sueldo = $sueldo;
        return $this;
    }

    public function setEmpresa($empresa) {
        $this->empresa = $empresa;
        return $this;
    }



}
