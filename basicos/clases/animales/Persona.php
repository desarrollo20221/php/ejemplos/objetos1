<?php

namespace clases\animales;

/*
 * Crear la clase Persona con las siguientes caracteristicas
 *
 * Propiedades publicas
 * nombre
 * apellidos
 * edad
 *
 * Getter y setter fluent
 *
 * constructor que inicializa todo
 *
 * destructor que muestra el texto destruido en la consola de javascript
 *
 * metodo toString que devuelve el nombre, los apellidos y la edad separados por comas
 *
 */

class Persona {
    public $nombre;
    public $apellidos;
    public $edad;
    
    public function __construct($nombre, $apellidos, $edad) {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
    }
    
    public function __destruct() {
        echo "Sacabo";
    }

    public function __toString() {
        return "{$this->nombre}, {$this->apellidos}, {$this->edad}";
    }
    
    
    public function getNombre() {
        return $this->nombre;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function getEdad() {
        return $this->edad;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
        return $this;
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
        return $this;
    }

    public function setEdad($edad) {
        $this->edad = $edad;
        return $this;
    }


    
}
