<?php

/**
 * Description of Coche
 *
 * @author miguel
 */
class Coche {
    // visibilidad (public,private,protected)
    public $tipo;
    private $matricula; // la matricula es privada
    public $cilindrada;
    public $fechaCompra;
    
    public function __construct($matricula) {
        $this->matricula = $matricula;
    }
    
    public function __toString() {
        $salida = "<ul>";
        $salida .= "<li>{$this->matricula}</li>";
        $salida .= "<li>{$this->tipo}</li>";
        $salida .= "</ul>";
        return $salida;
    }
    
    
    public function getTipo() {
        return $this->tipo;
    }

    public function getMatricula() {
        return $this->matricula;
    }

    public function getCilindrada() {
        return $this->cilindrada;
    }

    public function getFechaCompra() {
        return $this->fechaCompra;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
        return $this;
    }

    public function setMatricula($matricula) {
        $this->matricula = $matricula;
        return $this;
    }

    public function setCilindrada($cilindrada) {
        $this->cilindrada = $cilindrada;
        return $this;
    }

    public function setFechaCompra($fechaCompra) {
        $this->fechaCompra = $fechaCompra;
        return $this;
    }    
}
