<?php
// autocarga de clases
// cada vez que instancie un objeto llama a esta funcion
spl_autoload_register(function($nombreClase){
    //var_dump($nombreClase);
    require_once "./clases/$nombreClase.php";
    });

// quiero crear un objeto de tipo coche

$coche1 = new Coche("2323ABC");

var_dump($coche1);

$coche2 = new Coche("2525AAA");

var_dump($coche2);

