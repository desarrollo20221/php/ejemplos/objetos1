<?php
spl_autoload_register(function($nombreClase){    
    require_once "$nombreClase.php";
});

// importar las clases
use clases\animales\Persona;
use clases\animales\Empleado;

// crear persona
$persona1 = new Persona("Pepe", "Molla", 50);
// muestro la informacion sobre la persona con var_dump
var_dump($persona1);

// muestro la informacion de la persona con echo
echo $persona1;

// creo un empleado
$empleado1 = new Empleado(100, "alpe", "Jose", "Lopez", 36);

// muestro informacion sobre empleado1 con var_dump
var_dump($empleado1);

// muestro empleado con echo
echo $empleado1;

// presento al empleado con presentar
echo $empleado1->presentar();